import CartParser from './CartParser';
import { } from '../samples/test.csv';

let parser;

beforeEach(() => {
	parser = new CartParser();

});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	test('parseLine - valid name - should parse OK', () => {
		const inputLine = "Pepsi, 2.9, 2";
		const item = parser.parseLine(inputLine);
		expect(item.name).toBe("Pepsi");
	});

	test('parseLine - valid price - should parse OK', () => {
		const inputLine = "Pepsi, 2.90, 2";
		const item = parser.parseLine(inputLine);
		expect(item.price).toBe(2.90);

	});

	test('parseLine - valid quantity - should parse OK', () => {
		const inputLine = "Pepsi, 2.9, 2";
		const item = parser.parseLine(inputLine);
		expect(item.quantity).toBe(2);
	});

	test('parseLine - valid id format generated - should generate OK', () => {
		const inputLine = "Pepsi, 2.9, 2";
		const item = parser.parseLine(inputLine);
		const regexGuid = new RegExp("^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");
		expect(item.id).toMatch(regexGuid);
	});

	test('readFile - valid content - should read content OK', () => {
		const path = './samples/test.csv';
		const item = parser.readFile(path)
		expect(item).toMatch("test file content");
	});

	test('calcTotal - for empty cart - should calc OK', () => {
		const items = [];
		const sum = parser.calcTotal(items);
		expect(sum).toBe(0);
	});

	test('calcTotal - simple total calc for 1 item - should calc OK', () => {
		const inputLine = "Pepsi, 2.9, 2";
		const item = parser.parseLine(inputLine);
		const items = [];
		items.push(item);
		const sum = parser.calcTotal(items);
		expect(sum).toBe(5.8);
	});

	test('calcTotal - simple total calc for 2 items - should calc OK', () => {
		const inputLine = "Pepsi, 2.9, 2";
		const item1 = parser.parseLine(inputLine);
		const nextInputLine = "Cola, 3.1, 5";
		const item2 = parser.parseLine(nextInputLine);
		const items = [item1, item2];
		const sum = parser.calcTotal(items);
		expect(sum).toBe(21.3);
	});

	test('validate - no quantity in header - should give ERROR', () => {
		const inputLine = "Product name, Price";
		const validation = parser.validate(inputLine);
		const validationErrors = (validation || []).map(it => it.message);
		expect(validationErrors).toContain(`Expected header to be named \"Quantity"\ but received undefined.`);
	});

	test('validate - less cells - should give ERROR', () => {
		const inputLine = "Product name, Price, Quantity \n Candy, 2.5";
		const validation = parser.validate(inputLine);
		const validationErrors = (validation || []).map(it => it.message);
		expect(validationErrors).toContain(`Expected row to have 3 cells but received 2.`);
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	test('parse - valid content - should generate OK', () => {
		const path = './samples/validData.csv';
		const result = parser.parse(path)
		const items = result.items;
		const item1 = items[0];
		const item2 = items[1];
		expect(item1.name).toBe("Bread");
		expect(item2.name).toBe("Butter");
		expect(item1.price).toBe(2.05);
		expect(item2.price).toBe(4.32);
		expect(item1.quantity).toBe(2);
		expect(item2.quantity).toBe(1);
		expect(result.total).toBe(8.42);
	});
});